this["JST"] = this["JST"] || {};

this["JST"]["doc/templates/forum_post.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<!--topic 1-->\r\n<div class="row post impressions new hide-for-small">\r\n   <div class="small-12 columns">\r\n      <ul class="post-count-spacing small-block-grid-2 large-block-grid-2">\r\n         ';
 if (comments_count > 0) { ;
__p += '\r\n         <li>\r\n            <div class="post-count new already-checked">\r\n               <a href="#">\r\n               <div class="post-count-container new already-checked">\r\n                  <span class="reply-count new-post already-checked">NEW</span>\r\n               </div>\r\n               <div class="total-post-count new total-replies">\r\n                  ';
 if (comments_count == 1) { ;
__p += '\r\n                  ' +
((__t = ( comments_count )) == null ? '' : __t) +
' Reply\r\n                  ';
 } else { ;
__p += '\r\n                  ' +
((__t = ( comments_count )) == null ? '' : __t) +
' Replies\r\n                  ';
 } ;
__p += '\r\n               </div>\r\n               </a>\r\n            </div>\r\n         </li>\r\n         ';
 } else { ;
__p += '\r\n        <li>\r\n          <div class="post-count new-general">\r\n            <a href="' +
((__t = ( url )) == null ? '' : __t) +
'">\r\n              <div class="post-count-container new-general">\r\n                <span class="reply-count new-general">NEW</span>\r\n              </div>\r\n            </a>\r\n          </div>\r\n          <a href="' +
((__t = ( url )) == null ? '' : __t) +
'"></a>\r\n        </li>\r\n         ';
 } ;
__p += '\r\n         <li>\r\n           <div class="post-description">\r\n               <h5 class="content-title">\r\n                  <a href="' +
((__t = ( url )) == null ? '' : __t) +
'">' +
((__t = ( title || "Untitled" )) == null ? '' : __t) +
'</a>\r\n               </h5>\r\n               <p class="author-name">\r\n                   By\r\n                   <span class="author">\r\n                      <strong>\r\n                        ' +
((__t = ( author_name )) == null ? '' : __t) +
'\r\n                        ';
 if (typeof(latest_comment_author_name) != "undefined") { ;
__p += ',';
 } ;
__p += '\r\n                      </strong>\r\n                   </span>\r\n                   ';
 if (typeof(latest_comment_author_name) != "undefined") { ;
__p += '\r\n                   <span class="by">last Reply by</span>\r\n                   <span class="when">\r\n                      <span class="author">\r\n                         <strong>' +
((__t = ( latest_comment_author_name )) == null ? '' : __t) +
'</strong>\r\n                      </span>\r\n                      about ' +
((__t = ( latest_comment_created_at_time_ago )) == null ? '' : __t) +
' ago\r\n                   </span>\r\n                   ';
 } else { ;
__p += '\r\n                   <span class="when">about ' +
((__t = ( created_at_time_ago )) == null ? '' : __t) +
' ago</span>\r\n                   ';
 } ;
__p += '\r\n               </p>\r\n\r\n               <div class="post-description copy">\r\n                   <p>' +
((__t = ( body )) == null ? '' : __t) +
'</p>\r\n               </div>\r\n            </div>\r\n         </li>\r\n      </ul>\r\n      <hr>\r\n   </div>\r\n</div>\r\n<!-- end topic 1 large -->\r\n\r\n<!-- start  topic 1 small -->\r\n<div class="row post impressions new show-for-small">\r\n   <div class="small-12 columns">\r\n      <ul class="post-count-spacing small-block-grid-2 large-block-grid-2">\r\n        ';
 if (comments_count > 0) { ;
__p += '\r\n        <li>\r\n            <div class="post-count new already-checked">\r\n               <a href="' +
((__t = ( url )) == null ? '' : __t) +
'">\r\n               <div class="post-count-container new already-checked">\r\n                  <span class="reply-count new-post already-checked">NEW</span>\r\n               </div>\r\n               ';
 if (comments_count > 0) { ;
__p += '\r\n               <div class="total-post-count new total-replies">\r\n                  ';
 if (comments_count == 1) { ;
__p += '\r\n                  ' +
((__t = ( comments_count )) == null ? '' : __t) +
' Reply\r\n                  ';
 } else { ;
__p += '\r\n                  ' +
((__t = ( comments_count )) == null ? '' : __t) +
' Replies\r\n                  ';
 } ;
__p += '\r\n               </div>\r\n               ';
 } ;
__p += '\r\n               </a>\r\n            </div>\r\n        </li>\r\n        ';
 } else { ;
__p += '\r\n        <li>\r\n          <div class="post-count new-general">\r\n            <a href="' +
((__t = ( url )) == null ? '' : __t) +
'">\r\n              <div class="post-count-container new-general">\r\n                <span class="reply-count new-general">NEW</span>\r\n              </div>\r\n            </a>\r\n          </div>\r\n          <a href="' +
((__t = ( url )) == null ? '' : __t) +
'"></a>\r\n        </li>\r\n        ';
 } ;
__p += '\r\n        <li>\r\n            <h5 class="content-title">\r\n               <a href="' +
((__t = ( url )) == null ? '' : __t) +
'">' +
((__t = ( title || "Untitled" )) == null ? '' : __t) +
'</a>\r\n            </h5>\r\n            <p class="author-name">\r\n               By\r\n               <span class="author">\r\n                  <strong>\r\n                    ' +
((__t = ( author_name )) == null ? '' : __t) +
'\r\n                    ';
 if (typeof(latest_comment_author_name) != "undefined") { ;
__p += ',';
 } ;
__p += '\r\n                  </strong>\r\n               </span>\r\n               ';
 if (typeof(latest_comment_author_name) != "undefined") { ;
__p += '\r\n               <span class="by">last Reply by</span>\r\n               <span class="when">\r\n                  <span class="author">\r\n                     <strong>' +
((__t = ( latest_comment_author_name )) == null ? '' : __t) +
'</strong>\r\n                  </span>\r\n                  about ' +
((__t = ( latest_comment_created_at_time_ago )) == null ? '' : __t) +
' ago\r\n               </span>\r\n               ';
 } else { ;
__p += '\r\n               <span class="when">about ' +
((__t = ( created_at_time_ago )) == null ? '' : __t) +
' ago</span>\r\n               ';
 } ;
__p += '\r\n            </p>\r\n        </li>\r\n      </ul>\r\n      <div class="post-description">\r\n         <div class="post-description copy">\r\n\r\n             <p>Having issues installing with Compass and can\'t find a fix online. $ foundation new test Creating ./test exist test... (continued)</p>\r\n\r\n         </div>\r\n      </div>\r\n      <hr>\r\n   </div>\r\n</div>\r\n<!--end topic 1 small-->';

}
return __p
};